(ns lambda-calculus.core)

(defn cmp-sym [& syms] (let [set-syms (into #{} syms)] #(and (list? %) (contains? set-syms (first %)))))


(def lang-exit? (cmp-sym 'EXIT))
(def lang-lambda? (cmp-sym 'FN 'Λ))
(def lang-let? (cmp-sym 'LET))
(def lang-if? (cmp-sym 'IF))
(def lang-term? #(and (list? %) (not (or (lang-exit? %) (lang-lambda? %) (lang-let? %) (lang-if? %)))))


(defmulti compiler 
  (fn [expr]
    (cond
      (lang-exit? expr) :lang-exit
      (lang-lambda? expr) :lang-lambda
      (lang-let? expr) :lang-let
      (lang-if? expr) :lang-if
      (lang-term? expr) :lang-term
      :else :default)))

(defmethod compiler :lang-exit [_] (System/exit 0))

(defmethod compiler :lang-lambda [[_ var & body]] (list 'fn [var] (compiler body)))


(defmethod compiler :lang-let 
  [[_ var & body]]
  (eval (list 'def var (compiler body)))
  ())

(defmethod compiler :lang-if [[_ condition then else]] (list 'if (compiler condition) (compiler then) (compiler else)))


(defmethod compiler :lang-term [[first & rem]]
  (loop [acc (compiler first) [next & rem :as body] rem]
   (if (empty? body) acc (recur (list acc (compiler next)) rem))))


(defmethod compiler :default [expr] (if (and (list? expr) (= 1 (count expr))) (first expr) expr))


(defn format-line [line]
  (let* 
   [parenthesis (str "(" line ")")
    dot-replaced (clojure.string/replace parenthesis #"\." " ")
    spaced (clojure.string/replace dot-replaced #"(fn|λ)" "$1 ")
    line (read-string (clojure.string/upper-case spaced))] line))



(defmacro >> [& expr] (compiler (format-line expr)))


(defn interpreteur []
  (loop []
    (let*
      [line (format-line (read-line))
       value (compiler line)]
      ;; (println value)
      (println (eval value))
      (println)
      (recur))))

(defn -main
  [& args]
  (interpreteur))
