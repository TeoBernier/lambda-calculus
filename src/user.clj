(ns user)

(use 'lambda-calculus.core)

(def VALUE #((% true) false))

(def PRINT (fn [n] ((n #(+ 1 %)) 0)))

(>> let succ λ.n λ.f λ.x f (n f x))
(>> let plus λ.m λ.n m succ n)

(>> let zero λ.f λ.x x)
(>> let un succ zero)
(>> let deux succ un)
(>> let trois succ deux)
(>> let quatre succ trois)
(>> let cinq succ quatre)
(>> let six succ cinq)


(>> let mult λ.m λ.n m (plus n) zero)
(>> let pow λ.b λ.e e b)
(>> let pred λ.n λ.f λ.x n (λ.g λ.h h (g f)) (λ.u x) (λ.u u))
(>> let minus λ.m λ.n n pred m)


(>> let true λ.t λ.f t)
(>> let false λ.t λ.f f)
(>> let and λ.a λ.b a b a)
(>> let or λ.a λ.b a a b)
(>> let not λ.p p false true)
(>> let xor λ.a λ.b a (not b) b)
(>> let zero? λ.n n (λ.x false) true)


(>> let fact-loop λ.f λ.n (if (value (zero? n)) un (mult n (f f (pred n)))))
(>> let fact fact-loop fact-loop)

(>> let while-loop λ.loop λ.condition λ.f λ.vars λ.return (if (value (vars condition)) (loop loop condition f (vars f) return) (vars return)))
(>> let while while-loop while-loop)


(>> let sum λ.n (while (λ.i λ.acc not (zero? i)) (λ.i λ.acc (λ.f f (pred i) (plus i acc))) (λ.f f n zero) (λ.i λ.acc acc)))


